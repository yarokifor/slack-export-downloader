#!/usr/bin/env python3

import argparse
import os
import json
import requests
from urllib.parse import urlparse
import queue
import time
import threading

bytes_downloaded = []
existing_bytes = []
byte_correction = []


class WorkerItem:
    def __init__(self, url, full_file_path, size):
        self.url = url
        self.full_file_path = full_file_path
        self.size = size


def get_chat_files(directory):
    for a_file in os.listdir(directory):
        if a_file.endswith('.json'):
            yield open(os.path.join(directory, a_file), 'r', encoding='utf-8')


def worker(id, item_queue, stop_event):
    try:
        while True:
            headers = None
            mode = 'wb'
            local_size = 0
            item = item_queue.get(block=False)
            head_request = requests.head(item.url)

            if head_request.status_code != 200 and 'Content-Length' not in head_request.headers:
                byte_correction[id] -= item.size
                continue

            real_size = int(head_request.headers['Content-Length'])

            if real_size != item.size:
                byte_correction[id] += item.size - real_size

            try:
                stat = os.stat(item.full_file_path)
                local_size = stat.st_size
            except FileNotFoundError:
                pass
                 
            if local_size != real_size:
                if local_size < real_size:
                    headers = {'Range': 'bytes=%i-%i' % (local_size, real_size)}
                    mode = 'ab'
                    existing_bytes[id] += local_size

                try:
                    request = requests.get(item.url, headers=headers, stream=True, timeout=2)
                    with open(item.full_file_path, mode, buffering=0) as downloading_file:
                        total_received = 0
                        for chunk in request.iter_content(chunk_size=128):
                            length = len(chunk)
                            bytes_downloaded[id] += length
                            total_received += length
                            downloading_file.write(chunk)
                            if stop_event.isSet():
                                return
                except requests.exceptions.Timeout:
                    byte_correction[id] -= real_size
                    os.remove(item.full_file_path)
                    print('Timed out on %s' % item.full_file_path)
                    pass
                except Exception as e:
                    byte_correction[id] -= real_size
                    os.remove(item.full_file_path)
                    print('Thread %s had exception for item %s: %s' % (id, e, item.full_file_path))
                    pass
            else:
                existing_bytes[id] += local_size

            item_queue.task_done()
    except queue.Empty:
        pass 


def main():
    parser = argparse.ArgumentParser(description='Downloads all the file with in slack export channel.')
    parser.add_argument('export', default=None, type=str,
                        help='Directory with channels.json file and all channel directories')
    parser.add_argument('-c', default=None, type=str, action='append',
                        help='Only download files from theses channels')

    args = parser.parse_args()
    
    if args.export is None:
        print('No export directory given!')
        return

    print(args.c)

    export_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), args.export)

    if not os.path.isdir(export_path):
        print('Error path is not directory or does not exist.')
        return

    item_queue = queue.Queue()
    total_amount_of_files = 0
    total_amount_of_bytes = 0
    for channel in json.load(open(os.path.join(export_path, 'channels.json'),'r', encoding='utf-8')):
        channel_name = channel['name']
        if args.c is not None and channel_name not in args.c:
            continue

        channel_path = os.path.join(export_path, channel_name)
        
        if not os.path.isdir(channel_path):
            print('Channel %s directory does not exist skipping...'%channel_name)
            continue

        for chat_file in get_chat_files(channel_path):
            chat = json.load(chat_file)
            chat_file.close()
            save_path = os.path.join(channel_path, 'files')

            try:
                os.mkdir(save_path)
            except FileExistsError:
                pass

            for message in chat:
                if 'files' in message.keys() and len(message['files']) != 0:
                    for a_file in message['files']:
                        url = a_file['url_private_download']
                        size = a_file['size'] 
                        filename = urlparse(url).path.split('/')[-1]
                        split = filename.split('.', 1)
                        split[0] += '_%s'%a_file['id']
                        filename = '%s.%s'%(split[0],split[1])
                        item_queue.put(WorkerItem(url, os.path.join(save_path, filename), size))
                        total_amount_of_files += 1
                        total_amount_of_bytes += size 

    print('Starting downloading of %s files or about %0.1f Gigabytes...'%(total_amount_of_files, total_amount_of_bytes/(1000**3)))

    if total_amount_of_bytes <= 0:
        print('Done')
        return

    threads = []
    stop_event = threading.Event()
    for id in range(10):
        bytes_downloaded.append(0)
        existing_bytes.append(0)
        byte_correction.append(0)
        thread = threading.Thread(target=worker, args=(id, item_queue, stop_event))
        threads.append(thread)
        thread.start()

    try:
        living_threads = len(threads)
        last_bytes_amount = 0
        while living_threads != 0:
            total_bytes_downloaded = 0
            for i in bytes_downloaded:
               total_bytes_downloaded += i

            total_existing_bytes = 0
            for i in existing_bytes:
                total_existing_bytes += i

            total_byte_correction = 0
            for i in byte_correction:
                total_byte_correction += i

            bytes_delta = total_bytes_downloaded - last_bytes_amount
            last_bytes_amount = total_bytes_downloaded

            percentage = float(total_bytes_downloaded + total_existing_bytes) / float(total_amount_of_bytes + total_byte_correction)
            completed_fill = '█'*(int(100*percentage)%100)
            uncompleted_fill = '-'*(100-len(completed_fill))
            print('Progress: |%s%s| %s%% @%0.2f MB/s'% (completed_fill, uncompleted_fill, int(percentage*100), 8*bytes_delta/float(1000**2)), end='\r')
            time.sleep(1)
            living_threads = 0
            for thread in threads:
                if thread.is_alive():
                    living_threads += 1

        print('\nDone.')
    except KeyboardInterrupt:
        stop_event.set()
        print('\nStopping...')
        for thread in threads:
            thread.join()
           

if __name__ == '__main__':
    main()

